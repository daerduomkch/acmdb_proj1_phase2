# README #

This is my codes for a project on course **Database System** taught by [_Prof. Feifei Li_](http://www.cs.utah.edu/~lifeifei/) from the University of Utah in SJTU. In this project, I designed and implemented an online bookstore management system in PHP, JSP and Java.

# Contact:
Webpage: http://www.kaichun-mo.com