<%@ page language="java" import="acmdb.*" %>

<html>
	<head>
		<title>Customer Mode: Rate a feedback</title>
		<script src="Lib.js"></script>
		<script>
			function check(form_obj) {
				var res1 = checkString(form_obj.fblogin.value, 1, 20);
				var res2 = checkString(form_obj.fbisbn.value, 10, 10);
				var res3 = checkPosInt(form_obj.rate.value);
				var res4 = true;
				if(res3) {
					res4 = checkIntRange(form_obj.rate.value, 0, 10);
				}
				return res1 && res2 && res3 && res4;
			}
		</script>
	</head>

	<body>

		<%
			String login = request.getParameter("login");
		
			if(login == null) {
				
		%>
				<p>You need to login first!</p>
				<a href="index.jsp?choice=sign_in">Click here to login</a>
		<%
			} else {
				
				String filled = request.getParameter("filled");
				
				if(filled == null) {
					
		%>
		
			<p>You are accessing this bookstore with account <%=login %>!</p>

					<form name="UsefulRating" method="post" action="UsefulRating.jsp" onsubmit="return check(this)">
						<input type="hidden" name="login" value=<%=login %> />
						<input type="hidden" name="filled" value="true" />
						<table>
							<tr>
								<td>Feedback Author: </td>
								<td><input type="text" name="fblogin" size="40" maxLength="20" placeholder="whose feedback" /></td>
							</tr>
							<tr>
								<td>Feedback ISBN: </td>
								<td><input type="text" name="fbisbn" size="40" maxLength="10" placeholder="feedback book isbn" /></td>
							</tr>
							<tr>
								<td>Your Rating: </td>
								<td><input type="text" name="rate" size="40" maxLength="10" placeholder="your rating(0-10)" /></td>
							</tr>
						</table>
						<input type="submit" />
					</form>
					
					<a href="Customer.jsp?login=<%=login%>">Go Back</a>
			
		<%
				} else {
					
					String fblogin = request.getParameter("fblogin");
					String fbisbn = request.getParameter("fbisbn");
					int rate = Integer.parseInt(request.getParameter("rate"));
					
					Connector con = new Connector();
					
					// check the existence of the feedback
					CheckFeedback cf = new CheckFeedback();
					if(cf.run(fblogin, fbisbn, con.stmt)) {
					
						%>
							<p>ERROR: User <%=fblogin %> didn't give any feedback to book <%=fbisbn %>!</p>
							<a href="UsefulRating.jsp?login=<%=login %>">Go Back</a>
						<%
					} else {
					
						// regular
						AddUsefulRating aur = new AddUsefulRating();
						if(!aur.run(login, fblogin, fbisbn, rate, con.stmt)) {
							%>
								<p>ERROR: transanction failed! Maybe you have rate this feedback!</p>
								<a href="UsefulRating.jsp?login=<%=login %>">Go Back</a>
							<%
						} else {
							%>
								<p>Successfully rate this feedback! Thank you!</p>
								<a href="UsefulRating.jsp?login=<%=login %>">Go Back</a>
							<%
						}
					}
					
					con.closeConnection();
				}}
		%>


	</body>
</html>