<%@ page language="java" import="acmdb.*" %>

<html>
	<head>
		<title>Customer Mode</title>
	</head>

	<body>

		<%
			String login = request.getParameter("login");
		
			if(login == null) {
				
		%>
				<p>You need to login first!</p>
				<a href="index.jsp?choice=sign_in">Click here to login</a>
		<%
			} else {
				String func = request.getParameter("func");
				
				if(func == null) {
		%>
		
			<p>You are accessing this bookstore with account <%=login %>!</p>

					<form name="customer" method="get" action="Customer.jsp">
						<input type="hidden" name="login" value=<%=login %> />
						<input type="radio" name="func" value="Ordering" /> Order a book<br>
						<input type="radio" name="func" value="FeedbackRecording" /> Give feedback<br>
						<input type="radio" name="func" value="UsefulRating" /> Rate feedback<br>
						<input type="radio" name="func" value="TrustRecording" /> Trust some customers<br>
						<input type="radio" name="func" value="BookBrowsing" /> Browse books<br>
						<input type="radio" name="func" value="UsefulFeedback" /> Rate others' feedback<br>
						<input type="radio" name="func" value="DegreeOfSeparation" /> View degree of separation<br>
						<input type="submit">
					</form>
					
					<a href="index.jsp">Go Back</a>
			
		<%
		
				} else if(func.equals("Ordering")) {
					
					%>
						<jsp:forward page="Ordering.jsp">
							<jsp:param name="login" value='<%=login%>' />
						</jsp:forward>
					<%
					
				} else if(func.equals("FeedbackRecording")) {
					
					%>
						<jsp:forward page="FeedbackRecording.jsp">
							<jsp:param name="login" value='<%=login%>' />
						</jsp:forward>
					<%
					
				} else if(func.equals("UsefulRating")) {
					
					%>
						<jsp:forward page="UsefulRating.jsp">
							<jsp:param name="login" value='<%=login%>' />
						</jsp:forward>
					<%
					
				} else if(func.equals("TrustRecording")) {
					
					%>
						<jsp:forward page="TrustRecording.jsp">
							<jsp:param name="login" value='<%=login%>' />
						</jsp:forward>
					<%
						
				} else if(func.equals("BookBrowsing")) {
					
					%>
						<jsp:forward page="BookBrowsing.jsp">
							<jsp:param name="login" value='<%=login%>' />
						</jsp:forward>
					<%
					
				} else if(func.equals("UsefulFeedback")) {
					
					%>
						<jsp:forward page="UsefulFeedback.jsp">
							<jsp:param name="login" value='<%=login%>' />
						</jsp:forward>
					<%
					
				} else if(func.equals("DegreeofSeparation")) {
					
					%>
						<jsp:forward page="DegreeofSeparation.jsp">
							<jsp:param name="login" value='<%=login%>' />
						</jsp:forward>
					<%
					
				} else {
					
					%>
						<jsp:forward page="index.jsp">
							<jsp:param name="" value="" />
						</jsp:forward>
					<%
					
				}}
		%>


	</body>
</html>