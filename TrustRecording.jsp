<%@ page language="java" import="acmdb.*" %>

<html>
	<head>
		<title>Customer Mode: Order a book</title>
		<script src="Lib.js"></script>
		<script>
			function check(form_obj) {
				var res = checkString(form_obj.isbn.value, 1, 20);
				return res;
			}
		</script>
	</head>

	<body>

		<%
			String login = request.getParameter("login");
		
			if(login == null) {
				
		%>
				<p>You need to login first!</p>
				<a href="index.jsp?choice=sign_in">Click here to login</a>
		<%
			} else {
				
				String filled = request.getParameter("filled");
				
				if(filled == null) {
					
		%>
		
			<p>You are accessing this bookstore with account <%=login %>!</p>

					<form name="TrustRecord" method="post" action="TrustRecording.jsp" onsubmit="return check(this)">
						<input type="hidden" name="login" value=<%=login %> />
						<input type="hidden" name="filled" value="true" />
						<table>
							<tr>
								<td>His/Her login: </td>
								<td><input type="text" name="tarLogin" size="40" maxLength="20" placeholder="which user you want to trust/untrust" /></td>
							</tr>
							<tr>
								<td>Trust or Untrust: </td>
								<td><input type="radio" checked="checked" name="trust" value="1" /> Trust<br>
								<input type="radio" name="trust" value="0" /> Untrust<br></td>
							</tr>
						</table>
						<input type="submit" />
					</form>
					
					<a href="Customer.jsp?login=<%=login%>">Go back</a>
			
		<%
				} else {
					String tarLogin = request.getParameter("tarLogin");
					int trust = Integer.parseInt(request.getParameter("trust"));

					Connector con = new Connector();
					
					// check the existence of the book
					if(tarLogin.equals(login)) {
						%>
							<p>ERROR: you cannot trust yourself!</p>
							<p><a href="TrustRecording.jsp?login=<%=login%>">Go Back</a></p>
						<%
					} else {
					
						// check the quantity of the book
						AddTrust at = new AddTrust();
						if(!at.run(login, tarLogin, trust==1, con.stmt)) {

							%>
								<p>ERROR: you already trust or untrust him/her.</p>
								<p><a href="TrustRecording.jsp?login=<%=login%>">Go Back</a></p>
							<%
						}
						else {
								%>
								<p>Successfully add trust/untrust!</p>
								<p><a href="Customer.jsp?login=<%=login%>">Go Back</a></p>
								<%
						}
					}
					con.closeConnection();
				}}
		%>


	</body>
</html>