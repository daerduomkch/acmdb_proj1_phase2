<%@ page language="java" import="acmdb.*" %>

<html>
	<head>
		<title>Customer Mode: Order a book</title>
		<script src="Lib.js"></script>
		<script>
			function check(form_obj) {
				var res1 = checkString(form_obj.isbn.value, 10, 10);
				var res2 = checkPosInt(form_obj.quantity.value);
				return res1 && res2;
			}
		</script>
	</head>

	<body>

		<%
			String login = request.getParameter("login");
		
			if(login == null) {
				
		%>
				<p>You need to login first!</p>
				<a href="index.jsp?choice=sign_in">Click here to login</a>
		<%
			} else {
				
				String filled = request.getParameter("filled");
				
				if(filled == null) {
					
		%>
		
			<p>You are accessing this bookstore with account <%=login %>!</p>

					<form name="Ordering" method="post" action="Ordering.jsp" onsubmit="return check(this)">
						<input type="hidden" name="login" value=<%=login %> />
						<input type="hidden" name="filled" value="true" />
						<table>
							<tr>
								<td>Book ISBN: </td>
								<td><input type="text" name="isbn" size="50" placeholder="which book you want to order" /></td>
							</tr>
							<tr>
								<td>Quantity: </td>
								<td><input type="text" name="quantity" size="50" placeholder="how many copies to buy" /></td>
							</tr>
						</table>
						<input type="submit" />
					</form>
					
					<a href="Customer.jsp?login=<%=login%>">Go Back</a>
			
		<%
				} else {
					
					String isbn = request.getParameter("isbn");
					int quantity = Integer.parseInt(request.getParameter("quantity"));
					
					Connector con = new Connector();
					
					// check the existence of the book
					CheckBook cb = new CheckBook();
					if(!cb.run(isbn, con.stmt)) {
						%>
							<p>ERROR: book <%=isbn %> is not existed!</p>
							<p><a href="Ordering.jsp?login=<%=login%>">Go Back</a></p>
						<%
					} else {
					
						// check the quantity of the book
						CurrentBookQuantity cbq = new CurrentBookQuantity();
						int maxQuantity = cbq.run(isbn, con.stmt);
						if(quantity > maxQuantity) {
							%>
							<p>ERROR: book <%=isbn %> only has <%=maxQuantity %> copies!</p>
							<p><a href="Ordering.jsp?login=<%=login%>">Go Back</a></p>
							<%
						} else {
						
							// regular purchase
							Order order = new Order();
							try{
								order.run(login, isbn, quantity, con.stmt);
								%>
								<p>Successfully purchase <%=quantity%> copies of book <%=isbn %>!</p>
								<p><a href="Ordering.jsp?login=<%=login%>">Go Back</a></p>
								<%
							} catch(Exception e) {
								%>
								<p>ERROR: transaction failed!</p>
								<p><a href="Ordering.jsp?login=<%=login%>">Go Back</a></p>
								<%
							}
						}
					}
					
					con.closeConnection();
				}}
		%>


	</body>
</html>