<%@ page language="java" import="acmdb.*" %>

<html>
	<head>
		<title>Customer Mode</title>
		<script src="Lib.js"></script>
		<script>
			function check(form_obj) {
				var res = checkString(form_obj.isbn.value, 10, 10);
				if(res) {
					res = checkPosInt(form_obj.isbn.value);
					if (res) {
						res = checkPosInt(form_obj.rating.value);
						if (res) {
							res = checkIntRange(form_obj.rating.value, 0, 10);
							if (res) {
								res = checkString(form_obj.text.value, 0, 200);
							}
						}
					}
				}
				return res;
			}
		</script>
		
	</head>

	<body>

		<%
			String login = request.getParameter("login");
		
			if(login == null) {
				
		%>
				<p>You need to login first!</p>
				<a href="index.jsp?choice=sign_in">Click here to login</a>
		<%
			} else {
				String isbn = request.getParameter("isbn");
				String rating = request.getParameter("rating");
				String text = request.getParameter("text");
				int rate = 0;
				if(isbn == null) {
		%>
		
			    <p>Hi, <%=login %>! You are giving feedbacks now.</p>

				<form name="feedback" method=get onsubmit="return check(this)" action="FeedbackRecording.jsp">
					<input type=hidden name="login" value=<%=login %>>
					<table><tr>
					<td>isbn:</td><td><input type=text name="isbn" maxlength=<%=Lib.isbnLength%>> </td></tr>
					<tr><td>rating: </td><td><input type=text name="rating" maxlength=2>  </td></tr>
					<tr><td>text review: </td><td><input type=text name="text" maxlength=<%=Lib.feedbackTextMaxLength%>>  </td></tr>
					</table>
				<input type=submit>
				</form>
				
				<a href="Customer.jsp?login=<%=login%>">Go back</a>
			
		<%
				} else {
					Connector con = new Connector();
					boolean flag = true;
					CheckBook cb = new CheckBook();
					if (!cb.run(isbn, con.stmt)) {
						flag = false;
						out.println("This book doesn't exist.");
					}
					else {
						CheckFeedback cfb = new CheckFeedback();
						if (!cfb.run(login, isbn, con.stmt)) {
							flag = false;
							out.println("You have given feedback to this book whose isbn = "+isbn);
						}
					}
					if(text.length() == 0) text = null;
					if (flag) {
						rate = Integer.parseInt(rating);
						AddFeedback afb = new AddFeedback();
						afb.run(login, isbn, rate, text, con.stmt);
						out.println("Feedback succeeds.");
					}
					con.closeConnection();
				} 
			}
		%>

	</body>
</html>