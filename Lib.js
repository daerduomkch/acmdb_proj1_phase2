/**
 * This is some basic javascript functions to check the validity of the input
 */

function checkString(str, minLength, maxLength) {
	
	var length = str.length;
	
	if(length < minLength) {
		alert("ERROR: the input is too short! Cannot shorter than "+minLength);
		return false;
	}
	
	if(length > maxLength) {
		alert("ERROR: the input is too Long! Cannot longer than "+maxLength);
		return false;
	}
	
	if(str.match("\'") != null || str.match(" ") != null) {
		alert("ERROR: illegal character detected!");
		return false;
	}
	
	return true;
}

function checkFloat(input)
{
     var re = /^[0-9]+.?[0-9]*$/;       
     if (!re.test(input))
     {
        alert("ERROR: Not a Number!");
        return false;
     }
     return true;
}

function checkPosInt(input)
{
	
     var re = /^[1-9]+[0-9]*]*$/;

     if (!re.test(input))
     {
        alert("ERROR: Not a Positive Integer!");
        return false;
     }
     return true;
}

function checkIntRange(input, min, max) {
	
	if(input < min) {
		alert("ERROR: cannot smaller than "+min);
		return false;
	}
	
	if(input > max) {
		alert("ERROR: cannot larger than "+max);
		return false;
	}
	
	return true;
}
