<%@ page language="java" import="acmdb.*" %>

<html>
	<head>
		<title>Register</title>
		<script>

		    function check(form_obj) {
		    	
		    	var login = form_obj.username.value;
		    	var pwd = form_obj.pwd.value;
		    	var pwd2 = form_obj.pwd2.value;
		    	
				if (login == "") {
					alert("Please fill in your username!");
					return false;
				}
				if (pwd == "") {
					alert("Please fill in your password!");
					return false;
				}
				if (pwd!=pwd2) {
					alert("Two password is not identical!");
					return false;
				}
				
				return true;
		    }

   		 </script>
	</head>

	<body>

		<%
			String login = request.getParameter("username");
		
			if(login == null) {
		%>

			<form name="register" method="post" action="register.jsp" onsubmit="return check(this)">
				<table>
					<tr>
						<td>UserName</td>
					 	<td><input type="text" name="username" placeholder="Your username" maxlength="20"/> </td>
					</tr>
					<tr>
						<td>PassWord: </td>
						<td><input type="password" name="pwd"  placeholder="Your password" maxlength="20"/></td>
					</tr>
					<tr>
						<td>Confirm: </td>
						<td><input type="password" name="pwd2"  placeholder="Your password again" maxlength="20"/></td>
					</tr>
					<tr>
						<td>Name: </td>
						<td><input type="text" name="name" maxlength="20"/></td>
					</tr>
					<tr>
						<td>Address: </td>
						<td><input type="text" name="addr" maxlength="100" /></td>
					</tr>
					<tr>
						<td>Phone: </td>
						<td><input type="text" name="phone" maxlength="20"/></td>
					</tr>
				</table>
				<input type="submit" value="Register Now">
			</form>
			
		<%
		
			} else {
			
				String pwd = request.getParameter("pwd");
				String name = request.getParameter("name");
				String addr = request.getParameter("addr");
				String phone = request.getParameter("phone");
				
				acmdb.Connector con = new Connector();
				acmdb.InsertNewUser inu = new InsertNewUser();
	
				boolean res = inu.run(login, pwd, name, addr, phone, con.stmt);
	
				if(res) {
					out.println("Successfully registered!");
					out.println("<a href=\"index.jsp?choice=sign_in\">Click to login</a>");
				} else {
					out.println("The username has already been registered!");
					out.println("<a href=\"index.jsp?choice=register\">Click to register again</a>");
				}
				
				con.closeConnection();
			}
			%>


	</body>
</html>