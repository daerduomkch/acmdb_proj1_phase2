package acmdb;

import java.sql.*;

public class UsefulFeedback {
	
	UsefulFeedback() {
		
	}
	
	public Result run(String isbn, int k, Statement stmt) {
		
		String sql = "select avg(uf.rate) as avgScore, uf.fblogin "
				+ "from feedbacks fb, usefulnesses uf "
				+ "where fb.isbn = '"+isbn+"' and uf.fbisbn = '"+isbn+"' "
				+ "and fb.login = uf.fblogin "
				+ "group by uf.fblogin "
				+ "order by avg(uf.rate) desc";
		
		try {
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
			
			Statement stmt2 = Lib.con.con.createStatement();
			
			String[] title = {"count", "login", "rate", "text", "date"};
			
			rs.last();
			int size = rs.getRow();
			rs.beforeFirst();
			
			Object[][] data = new Object[size][5];
			
			int count = 0;
			
			while(rs.next()) {
				--k; ++count;
				
				if(k < 0) break;
				String fbLogin = rs.getString("fbLogin");
				
				String sql2 = "select * from feedbacks where isbn = '"+isbn+"' and login = '"+fbLogin+"'";
				
				System.out.println("Executing: "+sql2);
				ResultSet rs2 = stmt2.executeQuery(sql2);
				
				rs2.next();
				
				data[count-1][0] = count;
				data[count-1][1] = rs2.getString("login");
				data[count-1][2] = rs2.getInt("rate");
				data[count-1][3] = rs2.getString("text");
				data[count-1][4] = rs2.getDate("feedbackDate");
				
				rs2.close();
			}
			
			rs.close();
			
			return Lib.getResultInstance(size, 5, title, data);
			
		} catch (Exception e) {
			System.out.println("cannot execute the query");
			e.printStackTrace();
		}
		
		return null;
	}
}
