package acmdb;

import java.sql.ResultSet;
import java.sql.Statement;

public class Suggest {
	
	Suggest() {

	}
	
	public Result run(String isbn, Statement stmt) {
		String sql = "select o2.isbn, b.title, b.price, sum(o2.quantity) as total " +
				     "from orders o2, books b " +
				     "where o2.login in ( " +
				     "select o.login " +
				     "from orders o " +
				     "where o.isbn = '" + isbn + "') " +
				     "and b.isbn = o2.isbn "
				     + "group by o2.isbn, b.title, b.price "
				     + "order by total desc";
		try {
			String[] title = {"isbn", "title", "price", "total"};
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
			
			rs.last();
			int size = rs.getRow();
			rs.beforeFirst();

			Object[][] data = new Object[size][4];
			
			int count = 0;
			
			while(rs.next()) {
				++count;
				
				data[count-1][0] = rs.getString("isbn");
				data[count-1][1] = rs.getString("title");
				data[count-1][2] = rs.getFloat("price");
				data[count-1][3] = rs.getInt("total");
			}
			
			rs.close();

			return Lib.getResultInstance(size, 4, title, data);
		} catch (Exception e) {
			System.out.println("cannot execute the query");
			return null;
		}
	}
}
