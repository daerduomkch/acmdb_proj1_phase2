package acmdb;

import java.sql.*;

public class CustomerLogin {
	
	public CustomerLogin() {
		
	}
	
	public boolean run(String cid, String psw, Statement stmt) {
		
		// check malicious login, exclude special characters
		for(int i=0; i<cid.length(); ++i) {
			if(cid.charAt(i)=='\'' || cid.charAt(i) == '\"' || cid.charAt(i) == ' ') {
				System.out.println("Error: special character detected!");
				return false;
			}
		}
		
		for(int i=0; i<psw.length(); ++i) {
			if(psw.charAt(i)=='\'' || psw.charAt(i) == '\"' || psw.charAt(i) == ' ') {
				System.out.println("Error: special character detected!");
				return false;
			}
		}
		
		String sql = "select * from customers where login = '"+cid+"' and psw = '"+psw+"'";
		
		try {
			ResultSet rs = stmt.executeQuery(sql);
			return rs.next();
		} catch (Exception e) {
			System.out.println("cannot execute the query");
		}
		
		return false;
	}
}
