package acmdb;

import java.sql.*;

public class AddFeedback {

	public AddFeedback() {
		
	}
	
	public void run(String login, String isbn, int rate, String text, Statement stmt) {
		
		String sql = "insert into feedbacks (login, isbn, rate, feedbackDate) values"
				+ " ('"+login+"', '"+isbn+"', "+rate+", current_date())";
		
		if(text != null) {
			sql = "insert into feedbacks (login, isbn, rate, text, feedbackDate) "
					+ "values ('"+login+"', '"+isbn+"', "+rate+", '"+text+"', current_date())";
		}
		
		try {
			System.out.println("Executing: " + sql);
			stmt.executeUpdate(sql);
			
		} catch (Exception e){
			System.out.println("cannot execute the query");
		}
	}
}
