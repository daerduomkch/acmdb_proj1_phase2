package acmdb;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Lib {
	
	//customers related
	public static final int loginMaxLength = 20;
	public static final int passwordMaxLength = 20;
	public static final int addrMaxLength = 100;
	public static final int phoneMaxLength = 20;
	public static final int nameMaxLength = 20;
	
	//books related
	public static final int isbnLength = 10;
	public static final int titleMaxLength = 30;
	public static final int authorMaxLength = 30;
	public static final int publisherMaxLength = 30;
	public static final int keywordsMaxLength = 100;
	public static final int subjectMaxLength = 100;
	public static final int feedbackTextMaxLength = 200;
	
	// getString function
	public static String getString(int minLength, int maxLength, String mesg) {
		String res;
		while(true) {
			System.out.print(mesg);
			try {
				res = in.readLine();
				
				if(res.length() >= maxLength) {
					System.out.println("Error: too long! No more than " + (maxLength - 1) + " characters please!");
					continue;
				}
				if(res.length() < minLength) {
					System.out.println("Error: too short! No less than " + minLength + " characters please!");
					continue;
				}
				
				// no special characters
				for(int i=0; i<res.length(); ++i) {
					if(res.charAt(i) == ' ' || res.charAt(i) == '\'' || res.charAt(i) == '\"') {
						continue;
					}
				}
				
				return res;
			} catch(Exception e) {
				continue;
			}
		}
	}
	
	public static boolean checkString(String str, int minLength, int maxLength, String mesg) {
		try {
			if(str.length() >= maxLength) {
				mesg = "Error: too long! No more than " + (maxLength - 1) + " characters please!";
				return false;
			}
			if(str.length() < minLength) {
				mesg = "Error: too short! No less than " + minLength + " characters please!";
				return false;
			}
			for(int i=0; i<str.length(); ++i) 
				if (str.charAt(i) == ' ' || str.charAt(i) == '\'' || str.charAt(i) == '\"') {
					mesg = "Error: invalid character!";
					return false;
				}
		} catch(Exception e) {
			mesg = "Error !";
			return false;
		}
		return true;
	}
	
	// getInt function
	public static int getInt(int min, int max, String mesg) {
		
		while(true) {
			String res = getString(1, Integer.MAX_VALUE, mesg);
			
			try {
				int ans = Integer.parseInt(res);
				if(ans >= min && ans < max) return ans;
			} catch (Exception e) {
				continue;
			}
		}
	}
	
	public static int checkInt(String str, int min, int max, String mesg) {
		int ans;
		try {
			ans = Integer.parseInt(str);
			if (ans < min || ans >= max) { 
				mesg = "Error: out of bound!";
				return -1;
			}
		} catch(Exception e) {
			mesg = "Error !";
			return -1;
		}
		return ans;
	}
	
	// getFloat function
	public static double getFloat(double min, double max, String mesg) {
		
		while(true) {
			String res = getString(1, Integer.MAX_VALUE, mesg);
			
			try {
				double ans = Float.parseFloat(res);
				if (ans >= min && ans < max) return ans;
			} catch (Exception e) {
				continue;
			}
		}
	}
	
	public static double checkFloat(String str, int min, int max, String mesg) {
		double ans;
		try {
			ans = Float.parseFloat(str);
			if (ans < min || ans >= max) { 
				mesg = "Error: out of bound!";
				return -1;
			}
		} catch(Exception e) {
			mesg = "Error !";
			return -1;
		}
		return ans;
	}
	
	// getBoolean function
	public static boolean getBoolean(String trueStr, String falseStr, String mesg) {
		
		while(true) {
			String res = getString(1, Integer.MAX_VALUE, mesg);
			
			try {
				if(res.equals(trueStr)) return true;
				if(res.equals(falseStr)) return false;
			} catch (Exception e) {
				continue;
			}
		}
	}
	
	static Connector con;
	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	
	public static Result getResultInstance(int m, int n, String[] title, Object[][] data) {
		return new Result(m, n, title, data);
	}
	
	public static String getOutput(Result res) {
		
		String[][] transferredData = new String[res.m][res.n];
		
		int length[] = new int[res.n];
		for(int j=0; j<res.n; ++j) {
			length[j] = res.title[j].length();
			for(int i=0; i<res.m; ++i) {
				String tmp = "" + res.data[i][j];
				if(tmp.length() > length[j]) length[j] = tmp.length();
				transferredData[i][j] = tmp;
			}
			length[j] += 2;
		}
		
		String ans = __printFormat(length);
		ans += __printRow(length, res.title);
		ans += __printFormat(length);
		for(int i=0; i<res.m; ++i) {
			ans += __printRow(length, transferredData[i]);
		}
		ans += __printFormat(length);
		
		return ans;
	}
	
	private static String __printFormat(int[] length) {
		
		int n = length.length;
		
		String ans = "";
		for(int j=0; j<n; ++j) {
			ans += "+";
			for(int i=0; i<length[j]; ++i) {
				ans += '-';
			}
		}
		ans+="+\n";
		
		return ans;
	}
	
	private static String __printRow(int[] length, String[] row) {
		
		int n = length.length;
		
		String ans = "";
		for(int j=0; j<n; ++j) {
			ans += "|" + row[j];
			for(int i=row[j].length(); i<length[j]; ++i) {
				ans += " ";
			}
		}
		ans += "|\n";
		
		return ans;
	}
}

// Result class store all the output data
class Result {
	
	Result(int m, int n, String[] title, Object[][] data) {
		this.m = m;
		this.n = n;
		this.title = title;
		this.data = data;
	}
		
	// m rows(without count of the title row), n columns
	int m, n;
		
	String[] title;
	Object[][] data;
}