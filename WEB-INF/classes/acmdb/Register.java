package acmdb;

import java.io.*;
import java.sql.*;

public class Register {
	
	Register(BufferedReader in, Statement stmt) {
		this.in = in;
		this.stmt = stmt;
	}
	
	public boolean run() {
		
		System.out.println(prefix + "Welcome to register in our bookstore!");
		
		// login
		String login;
		while(true) {
			
			login = Lib.getString(1, Lib.loginMaxLength, prefix + "your login: ");
			
			// guarantee the uniqueness of string login
			CheckForLoginUniqueness check = new CheckForLoginUniqueness();
			if(check.run(login, stmt)) break;
			else {
				System.out.println(prefix + login + " has been used. Please try another login!");
			}
		}
		
		// password
		String psw;
		while(true) {
			
			psw = Lib.getString(1, Lib.passwordMaxLength, prefix + "your password: ");
			
			// enter psd again
			boolean flag;
			String psw2 = Lib.getString(0, Integer.MAX_VALUE, prefix + "your password again: ");
			
			// check whether psw equals psw2
			if(!psw.equals(psw2)) {
				flag = false;
				System.out.println(prefix + "the two password is not identical!");
			} else {
				flag = true;
			}
			
			if(flag) break;
		}
		
		// other informations
		String name = Lib.getString(0, Lib.nameMaxLength, prefix + "(Not Required) "+"your name: ");
		String addr = Lib.getString(0, Lib.addrMaxLength, prefix + "(Not Required) "+"your address: ");
		String phone = Lib.getString(0, Lib.phoneMaxLength, prefix + "(Not Required) "+"your phone number: ");
		
		InsertNewUser inu = new InsertNewUser();
		return inu.run(login, psw, name, addr, phone,stmt);
	}
	
	String prefix = "Registration: > ";
	BufferedReader in;
	Statement stmt;
}
