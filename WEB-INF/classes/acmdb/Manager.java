package acmdb;

import java.io.*;
import java.sql.*;
import java.util.LinkedList;

public class Manager {
	
	Manager(BufferedReader in, Statement stmt) {
		this.in = in;
		this.stmt = stmt;
	}
	
	public void run() {
		
		welcome();
		checkIdentity();
		
		while(true) {
			int opt = chooseOption();
			
			try {
				switch(opt) {
				
					case newBook: 
						newBook(); continue;
					case newCopies:
						newCopies(); continue;
					case statistics:
						statistics(); continue;
					case userAwards:
						userAwards(); continue;
					default: 	// exit
						return;
				}
			} catch (Exception e) {
				System.out.println(prefix + "Error Occurred!");
			}
		}
		
	}
	
	private void newBook() {
		
		String isbn = Lib.getString(Lib.isbnLength, Lib.isbnLength + 1, prefix + "isbn: ");
		String title = Lib.getString(0, Lib.titleMaxLength + 1, prefix + "title: ");
		
		int authorNumber = Lib.getInt(0, Integer.MAX_VALUE, prefix + "author number: ");
		LinkedList<String> authorList = new LinkedList<String>();
		for(int i=0; i<authorNumber; ++i)	{
			String author = Lib.getString(1, Lib.authorMaxLength + 1, prefix + "author "+(i+1)+" name: ");
			authorList.add(author);
		}
		
		String publisher = Lib.getString(0, Lib.publisherMaxLength + 1, prefix + "publisher: ");
		int yop = Lib.getInt(1500, 2100, prefix + "year of public: ");
		double price = Lib.getFloat(0, Float.MAX_VALUE, prefix + "price: ");
		int quantity = Lib.getInt(0, Integer.MAX_VALUE, prefix + "quantity: ");
		boolean format = Lib.getBoolean("hardcover", "softcover", prefix + "format(hardcover/softcover): ");
		String keywords = Lib.getString(0, Lib.keywordsMaxLength + 1, prefix + "keywords: ");
		String subject = Lib.getString(0, Lib.subjectMaxLength + 1, prefix + "subject: ");
		
		InsertNewBook inb = new InsertNewBook();
		inb.run(isbn, authorList, yop, publisher, title, price, format, keywords, subject, quantity, stmt);
	}
	
	private void newCopies() {
		
		String isbn = Lib.getString(Lib.isbnLength, Lib.isbnLength + 1, prefix + "isbn: ");
		int incr = Lib.getInt(1, Integer.MAX_VALUE, prefix + "number of copies to increase: ");
		
		CheckBook cb = new CheckBook();
		
		if (!cb.run(isbn, stmt)) {
			System.out.println("This book doesn't exist.");
			return;
		}
		
		InsertNewCopies inc = new InsertNewCopies();
		inc.run(isbn, incr, stmt);
	}

	private void statistics() {
		int m = Lib.getInt(1, Integer.MAX_VALUE, prefix + "number of top popular: ");
		Statistics st = new Statistics();
		System.out.println("Top " + m + " popular books:\n" + Lib.getOutput(st.run(1, m, stmt)));
		System.out.println("Top " + m + " popular authors:\n" + Lib.getOutput(st.run(2, m, stmt)));
		System.out.println("Top " + m + " popular publishers:\n" + Lib.getOutput(st.run(3, m, stmt)));
	}

	private void userAwards() {
		int m = Lib.getInt(1, Integer.MAX_VALUE, prefix + "number of top users: ");
		Award aw = new Award();
		System.out.println("Top " + m + " most trusted users:\n" + Lib.getOutput(aw.run(1, m, stmt)));
		System.out.println("Top " + m + " most useful users:\n" + Lib.getOutput(aw.run(2, m, stmt)));
	}
	
	private void welcome() {
		System.out.println("\n\t Here is manager mode!");
	}
	
	private boolean checkIdentity() {
		
		String mid, psw;
		while(true) {
			System.out.println();
			
			mid = Lib.getString(1, Integer.MAX_VALUE, prefix + "your manager id: ");
			psw = Lib.getString(1, Integer.MAX_VALUE, prefix + "your password: ");
			
			ManagerLogin ml = new ManagerLogin();
			if(ml.run(mid, psw, stmt)) break;
			else {
				System.out.println(prefix + "wrong manager id or password, please check!");
			}
		}
		
		return true;
	}
	
	private int chooseOption() {
		
		System.out.println();
		boolean next = Lib.getBoolean("y", "n", "Press y to continue: ");
		if(!next) return 0;
		
		while(true) {
			
			System.out.println("\nWhat you want to do?");
			System.out.println("\t"+newBook+". add new book");
			System.out.println("\t"+newCopies+". add new copies");
			System.out.println("\t"+statistics+". get statistics");
			System.out.println("\t"+userAwards+". see best users");
			System.out.println("\t"+exit+". exit system");
			
			try {
				System.out.print("> ");
				String choice = in.readLine();
				int res = Integer.parseInt(choice);
				if(res < 0 || res > 5) throw new Exception();
				return res;
			} catch(Exception e) {
				System.out.println("> ChooseOption Failure: invalid choice!\n");
				continue;
			}
		}
	}
	
	String prefix = "Manager Mode > ";
	
	Statement stmt;
	BufferedReader in;
	
	static final int newBook = 1;
	static final int newCopies = 2;
	static final int statistics = 3;
	static final int userAwards = 4;
	static final int exit = 0;
}
