package acmdb;

import java.sql.ResultSet;
import java.sql.Statement;

public class Award {
	
	Award() {

	}
	
	public Result run(int i, int m, Statement stmt) {
		switch (i) {
			case(1):
				return mostTrusted(m, stmt);
			case(2):
				return mostUseful(m, stmt);
			default:
				return null;
		}
	}

	private Result mostTrusted(int m, Statement stmt) {
		String sql = "select c.login"
				+ " from customers c"
				+ " order by ("
				+ " select count(*)"
				+ " from trusts t"
				+ " where t.tarLogin = c.login and t.trust = true )"
				+ " - ( select count(*)"
				+ " from trusts t"
				+ " where t.tarLogin = c.login and t.trust = false"
				+ " ) desc";
		try {
			String[] title = {"user's login"};
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
		
			rs.last();
			int size = rs.getRow();
			rs.beforeFirst();

			Object[][] data = new Object[size][1];
		
			int count = 0;
		
			while(rs.next()) {
				++count;
			
				data[count-1][0] = rs.getString("login");
				
				if (count == m) break;
			}
		
			rs.close();

			return Lib.getResultInstance(count, 1, title, data);
		} 
		catch (Exception e) {
			System.out.println("cannot execute the query");
			return null;
		}
	}

	private Result mostUseful(int m, Statement stmt) {
		String sql = "select tmp.fbLogin, avg(tmp.aver)"
				+ " from ("
				+ " select u.fbLogin, u.fbisbn, avg(u.rate) as aver"
				+ " from usefulnesses u"
				+ " group by u.fbLogin, u.fbisbn ) as tmp"
				+ " group by tmp.fbLogin"
				+ " order by avg(tmp.aver) desc";
		try {
			String[] title = {"user's login"};
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
		
			rs.last();
			int size = rs.getRow();
			rs.beforeFirst();

			Object[][] data = new Object[size][1];
		
			int count = 0;
		
			while(rs.next()) {
				++count;
			
				data[count-1][0] = rs.getString("fbLogin");
				
				if (count == m) break;
			}
		
			rs.close();

			return Lib.getResultInstance(count, 1, title, data);
		} 
		catch (Exception e) {
			System.out.println("cannot execute the query");
			return null;
		}
	}
}
