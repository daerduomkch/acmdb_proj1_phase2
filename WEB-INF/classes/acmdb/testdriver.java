package acmdb;

public class testdriver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		welcome();
		connection();
		
		int id = chooseIdentity();
		
		if(id == 0) terminate();
		
		if(id == register) {
			Register reg = new Register(Lib.in, Lib.con.stmt);
			if(reg.run()) id = signIn;
		}
		
		try {
			if(id == signIn) {
				Customer cus = new Customer(Lib.in, Lib.con.stmt);
				cus.run();
			} else {
				Manager man = new Manager(Lib.in, Lib.con.stmt);
				man.run();
			}
		} catch (Exception e) {
			System.out.println("> Runtime Error: mode is " + ((id == signIn)? "customer":"manager"));
			e.printStackTrace();
		} finally {
			terminate();
		}
	}

	static void welcome() {
		System.out.println("\tWelcome to acmdb06 online bookstore\n");
	}
	
	static void connection() {
		
		System.out.println("> Connection: Connecting to the database server, please wait...");
		
		try {
			Lib.con = new Connector();
		} catch(Exception e) {
			System.out.println("> Connection Failure: Cannot connect to database server!\n");
			e.printStackTrace();
		}
		
		System.out.println("> Connection: Successfully connected to database server!\n");
	}
	
	static void terminate() {
		if (Lib.con != null) {
			try {
				Lib.con.closeConnection();
				System.out.println ("> Termination: Database connection terminated");
			} catch (Exception e) { 
				System.out.println("> Termination Failure: Database connection is not terminated successfully!");
			}
   	 	}
		
		System.exit(0);
	}
	
	static int chooseIdentity() {
		
		while(true) {
			
			System.out.println("Identity check(choose one from the following options):");
			System.out.println("\t" + signIn + ". sign in");
			System.out.println("\t" + register + ". register");
			System.out.println("\t" + manager + ". manager");
			System.out.println("\t" + exit + ". exit");
			
			try {
				System.out.print("> ");
				String choice = Lib.in.readLine();
				int res = Integer.parseInt(choice);
				if(res < 0 || res > 3) throw new Exception();
				return res;
			} catch(Exception e) {
				System.out.println("> CheckIdentity Failure: invalid choice!\n");
				continue;
			}
		}
	}
	
	static final int signIn = 1;
	static final int register = 2;
	static final int manager = 3;
	static final int exit = 0;
}
