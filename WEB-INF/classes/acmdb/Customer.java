package acmdb;

import java.io.*;
import java.sql.*;

public class Customer {
	
	Customer(BufferedReader in, Statement stmt) {
		this.in = in;
		this.stmt = stmt;
	}
	
	public void run() {
		
		welcome();
		checkIdentity();
		
		while(true) {
			int opt = chooseOption();
			
			try {
				switch(opt) {
				
					case ordering:
						ordering(); continue;
					case feedbackRecording:
						feedbackRecording(); continue;
					case usefulRating:
						usefulRating(); continue;
					case trustRecording:
						trustRecording(); continue;
					case bookBrowsing:
						bookBrowsing(); continue;
					case usefulFeedback:
						usefulFeedback(); continue;
					case degreeOfSeperation:
						degreeOfSeperation(); continue;
					default: 	// exit
						return;
				}
			} catch (Exception e) {
				System.out.println(prefix + "Error Occurred!");
			}
		}
		
	}
	
	private void ordering() {
		String prompt = "User Mode: Ordering > ";
		
		String isbn = Lib.getString(Lib.isbnLength, Lib.isbnLength + 1, prompt + "isbn: ");
		
		CurrentBookQuantity cbq = new CurrentBookQuantity();
		int largestAmount = cbq.run(isbn, stmt);
		if(largestAmount == 0) {
			System.out.println(prompt + "book "+isbn+" does not exist!");
			return;
		}
		int number = Lib.getInt(1, largestAmount + 1, prompt + "copies you want to buy(<="+largestAmount+"):");
		
		Order order = new Order();
		order.run(login, isbn, number, stmt);
		buyingSuggestions(isbn);
	}

	private void feedbackRecording() {
		String prompt = "User Mode: Feedback > ";
		
		String isbn = Lib.getString(Lib.isbnLength, Lib.isbnLength + 1, prompt + "isbn: ");
		
		CheckBook cb = new CheckBook();
		
		if (!cb.run(isbn, stmt)) {
			System.out.println(prompt + "This book doesn't exist.");
			return;
		}
		
		CheckFeedback cfb = new CheckFeedback();
		
		if(cfb.run(login, isbn, stmt)) {	// hasn't written feedback
			int rate = Lib.getInt(0, 11, prompt + "rating(0-10): ");
			String text = Lib.getString(0, Lib.feedbackTextMaxLength + 1, prompt + "(optional) text review: ");
			if(text.length() == 0) text = null;
			
			AddFeedback afb = new AddFeedback();
			afb.run(login, isbn, rate, text, stmt);
		} else {	// has written feedback
			System.out.println(prompt + "You have give feedback to this book whose isbn = "+isbn);
		}
	}

	private void usefulRating() {
		String prompt = "User Mode: Rating Other Feedback > ";
		
		String fblogin = Lib.getString(1, Lib.loginMaxLength + 1, prompt + "his/her login: ");
		String fbisbn = Lib.getString(Lib.isbnLength, Lib.isbnLength + 1, prompt + "which book: ");
		int rating = Lib.getInt(0, 3, prompt + "rating(0/1/2): ");
		
		if(fblogin.equals(login)) {
			System.out.println(prompt + "ERROR: you cannot score your own review!");
			return;
		}
		
		CheckFeedback cfb = new CheckFeedback();
		if (cfb.run(fblogin, fbisbn, stmt)) {
			System.out.println(prompt + "ERROR: this review doesn't exist!");
			return;
		}
		
		AddUsefulRating aur = new AddUsefulRating();
		if(!aur.run(login, fblogin, fbisbn, rating, stmt)) {
			System.out.println(prompt + "ERROR: you have rated this review!");
		}
	}

	private void trustRecording() {
		String prompt = "User Mode: Trust Other User > ";
		
		String tarLogin = Lib.getString(1, Lib.loginMaxLength + 1, prompt + "his/her login: ");
		if(tarLogin.equals(login)) {
			System.out.println(prompt + "ERROR: you cannot trust yourself!");
			return;
		}
		
		boolean trust = Lib.getBoolean("trust", "untrust", prompt + "trust or untrust: ");
		
		AddTrust at = new AddTrust();
		if(!at.run(login, tarLogin, trust, stmt)) {
			System.out.println(prompt + "ERROR: you already trust or untrust him/her.");
		}
	}

	private void bookBrowsing() {
		String prompt = "User Mode: Book Browsing > ";
		String tarAuthor = Lib.getString(0, Lib.authorMaxLength + 1, prompt + "auther name: ");
		String tarPublisher = Lib.getString(0, Lib.publisherMaxLength + 1, prompt + "publisher: ");
		String tarTitle = Lib.getString(0, Lib.titleMaxLength + 1, prompt + "title-words: ");
		String tarSubject = Lib.getString(0, Lib.subjectMaxLength + 1, prompt + "subject: ");
		String blank = "\t\t\t   ";
		int tarSort = Lib.getInt(1, 3 + 1, prompt + "sort the result by\n" +
		                                   blank + "1.year\n" +
		                                   blank + "2.the average numerical score of the feedbacks\n" +
		                                   blank + "3.the average numerical score of the trusted user feedbacks\n" +
		                                   blank + "Enter 1, 2 or 3:"); 
		System.out.println(prompt + "Search result: ");
		
		Browse br = new Browse();
		Result res = br.run(tarAuthor, tarPublisher, tarTitle, tarSubject, tarSort, login, stmt);
		System.out.println(Lib.getOutput(res));
	}

	private void usefulFeedback() {
		String prompt = "User Mode: Browse Useful Feedbacks > ";
		
		String isbn = Lib.getString(Lib.isbnLength, Lib.isbnLength + 1, prompt + "isbn: ");
		int k = Lib.getInt(1, Integer.MAX_VALUE, prompt + "first kth feedbacks: ");
		
		UsefulFeedback uf = new UsefulFeedback();
		Result res = uf.run(isbn, k, stmt);
		System.out.println(Lib.getOutput(res));
	}

	private void buyingSuggestions(String isbn) {
		System.out.println("Suggesting books list: ");

		Suggest sg = new Suggest();
		Result res = sg.run(isbn, stmt);
		System.out.println(Lib.getOutput(res));
	}

	private void degreeOfSeperation() {
		String prompt = "User Mode: Degree of Seperation > ";
		String tarAuthor1 = Lib.getString(1, Lib.authorMaxLength + 1, prompt + "First auther name: ");
		String tarAuthor2 = Lib.getString(1, Lib.authorMaxLength + 1, prompt + "Second auther name: ");
		Seperation sp = new Seperation();
		String deg = sp.run(tarAuthor1, tarAuthor2, stmt);
		System.out.println("The degree of seperation between " 
		                   + tarAuthor1 + " and " + tarAuthor2 + " is " + deg + ".");
	}

	
	private void welcome() {
		System.out.println("\n\t Here is user mode!");
	}
	
	private boolean checkIdentity() {
		
		String uid, psw;
		while(true) {
			System.out.println();
			
			uid = Lib.getString(1, Integer.MAX_VALUE, prefix + "your id: ");
			psw = Lib.getString(1, Integer.MAX_VALUE, prefix + "your password: ");
			
			CustomerLogin cl = new CustomerLogin();
			if(cl.run(uid, psw, stmt)) {
				login = uid;
				break;
			}
			else {
				System.out.println(prefix + "wrong id or password, please check!");
			}
		}
		
		return true;
	}
	
	private int chooseOption() {
		
		System.out.println();
		boolean next = Lib.getBoolean("y", "n", "Press y to continue: ");
		if(!next) return 0;
		
		while(true) {
			
			System.out.println("\nWhat you want to do?");
			System.out.println("\t"+ordering+". ordering");
			System.out.println("\t"+feedbackRecording+". feedbackRecording");
			System.out.println("\t"+usefulRating+". usefulRating");
			System.out.println("\t"+trustRecording+". trustRecording");
			System.out.println("\t"+bookBrowsing+". bookBrowsing");
			System.out.println("\t"+usefulFeedback+". usefulFeedback");
			System.out.println("\t"+degreeOfSeperation+". degreeOfSeperation");
			System.out.println("\t"+exit+". exit");
			
			try {
				System.out.print("> ");
				String choice = in.readLine();
				int res = Integer.parseInt(choice);
				if(res < 0 || res > 8) throw new Exception();
				return res;
			} catch(Exception e) {
				System.out.println("> ChooseOption Failure: invalid choice!\n");
				continue;
			}
		}
	}
	
	String prefix = "User Mode > ";
	
	Statement stmt;
	BufferedReader in;
	
	String login;
	
	static final int ordering = 1;
	static final int feedbackRecording = 2;
	static final int usefulRating = 3;
	static final int trustRecording = 4;
	static final int bookBrowsing = 5;
	static final int usefulFeedback = 6;
	static final int degreeOfSeperation = 7;
	static final int exit = 0;
}
