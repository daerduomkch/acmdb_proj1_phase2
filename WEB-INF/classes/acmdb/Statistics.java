package acmdb;

import java.sql.ResultSet;
import java.sql.Statement;

public class Statistics {

	public Result run(int i, int m, Statement stmt) {
		switch (i) {
			case(1):
				return topBooks(m, stmt);
			case(2):
				return topAuthors(m, stmt);
			case(3):
				return topPublishers(m, stmt);
			default:
				return null;
		}
	}

	private Result topBooks(int m, Statement stmt) {
		String sql = "select o.isbn, sum(o.quantity) as total"
				+ " from orders o"
				+ " where current_date() - o.orderDate <= 90"
				+ " group by o.isbn"
				+ " order by sum(o.quantity) desc";
		try {
			String[] title = {"isbn", "total"};
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
		
			rs.last();
			int size = rs.getRow();
			rs.beforeFirst();

			Object[][] data = new Object[size][2];
		
			int count = 0;
		
			while(rs.next()) {
				++count;
			
				data[count-1][0] = rs.getString("isbn");
				data[count-1][1] = rs.getInt("total");
				
				if (count == m) break;
			}
		
			rs.close();

			return Lib.getResultInstance(count, 2, title, data);
		} 
		catch (Exception e) {
			System.out.println("cannot execute the query");
			return null;
		}
	}

	private Result topAuthors(int m, Statement stmt) {
		String sql = "select w.name, sum(o.quantity) as total"
				+ " from orders o, writes w"
				+ " where o.isbn = w.isbn"
				+ " and current_date() - o.orderDate <= 90"
				+ " group by w.name"
				+ " order by sum(o.quantity) desc";
		try {
			String[] title = {"Author", "total"};
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
		
			rs.last();
			int size = rs.getRow();
			rs.beforeFirst();

			Object[][] data = new Object[size][2];
		
			int count = 0;
		
			while(rs.next()) {
				++count;
			
				data[count-1][0] = rs.getString("name");
				data[count-1][1] = rs.getInt("total");
				
				if (count == m) break;
			}
		
			rs.close();

			return Lib.getResultInstance(count, 2, title, data);
		} 
		catch (Exception e) {
			System.out.println("cannot execute the query");
			return null;
		}
	}

	private Result topPublishers(int m, Statement stmt) {

		String sql = "select b.publisher, sum(o.quantity) as total"
				+ " from orders o, books b"
				+ " where o.isbn = b.isbn"
				+ " and current_date() - o.orderDate <= 90"
				+ " group by b.publisher"
				+ " order by sum(o.quantity) desc";
		try {
			String[] title = {"publisher", "total"};
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
		
			rs.last();
			int size = rs.getRow();
			rs.beforeFirst();

			Object[][] data = new Object[size][2];
		
			int count = 0;
		
			while(rs.next()) {
				++count;
			
				data[count-1][0] = rs.getString("publisher");
				data[count-1][1] = rs.getInt("total");
				
				if (count == m) break;
			}
		
			rs.close();

			return Lib.getResultInstance(count, 2, title, data);
		} 
		catch (Exception e) {
			System.out.println("cannot execute the query");
			return null;
		}
	}

}
