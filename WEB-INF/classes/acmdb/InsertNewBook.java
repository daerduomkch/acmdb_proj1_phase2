package acmdb;

import java.sql.*;
import java.util.LinkedList;

public class InsertNewBook {
	
	InsertNewBook() {
		
	}
	
	public void run(String isbn, LinkedList<String> authorList, int yop, String publisher, String title, double price,
			boolean format, String keywords, String subject, int quantity, Statement stmt) {
		
		// add this book
		String sql = "insert into books (isbn, yop, publisher, title, price, "
				+ "format, keywords, subject, quantity) values ('"+isbn+"', "+yop+", '"
				+publisher+"', '"+title+"', "+price+", "+format+", '"+keywords+"', '"+
				subject+"', "+quantity+")";
		
		try {
			System.out.println("Executing: "+sql);
			stmt.executeUpdate(sql);	
			
		} catch (Exception e) {
			System.out.println("cannot execute the query");
			return;
		}
		
		System.out.println(authorList);
		// add all authors
		for(String author: authorList) {
			
			sql = "insert into authors (name) values ('"+author+"')";
			
			try {
				System.out.println("Executing: "+sql);
				stmt.executeUpdate(sql);
				
			} catch (Exception e) {
				System.out.println("cannot execute the query");
			}
		}
		
		// add all write relations
		for(String author: authorList) {
			
			sql = "insert into writes (name, isbn) values ('"+author+"', '"+isbn+"')";
			
			try {
				System.out.println("Executing: "+sql);
				stmt.executeUpdate(sql);
				
			} catch (Exception e) {
				System.out.println("cannot execute the query");
			}
		}
	}
}
