package acmdb;

import java.sql.*;

public class Order{
	
	public Order() {
		
	}
	
	public void run(String login, String isbn, int number, Statement stmt) {
		
		// delete number copies from this book record
		String sql = "update books set quantity = quantity - "+number+" where isbn = '"+isbn+"'";
		
		try{
			System.out.println("Executing: "+sql);
			stmt.executeUpdate(sql);
        } catch(Exception e) {
        	System.out.println("cannot execute the query");
        	return;
		}
		
		// check whether want to re-buy or not
		sql = "select * from orders where login = '"+login+"' and isbn = '"+isbn+"' "
				+ "and orderDate = current_date()";
		
		boolean flag = false;
		try{
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
			flag = rs.next();
        } catch(Exception e) {
        	System.out.println("cannot execute the query");
        	e.printStackTrace();
		}
		
		if(flag) { // re-buy on this day
			
			sql = "update orders set quantity = quantity + "+number+" where isbn = '"+isbn+"' "
					+ "and login = '"+login+"' and orderDate = current_date()";
			
			try{
				System.out.println("Executing: "+sql);
				stmt.executeUpdate(sql);
			} catch(Exception e) {
				System.out.println("cannot execute the query");
				e.printStackTrace();
			}
			
		} else { //new order on this day
			
			sql = "insert into orders (login, isbn, quantity, orderDate) values ('"
					+login+"', '"+isbn+"', "+number+", current_date())";
			
			try{
				System.out.println("Executing: "+sql);
				stmt.executeUpdate(sql);
			} catch(Exception e) {
				System.out.println("cannot execute the query");
				e.printStackTrace();
			}
		}
	}
}
