package acmdb;

import java.sql.ResultSet;
import java.sql.Statement;

public class Seperation {
	Seperation() {

	}
	
	public String run(String tarAuthor1, String tarAuthor2, Statement stmt) {
		tarAuthor1 = "'" + tarAuthor1 + "'";
		tarAuthor2 = "'" + tarAuthor2 + "'";
		if (oneDegree(tarAuthor1, tarAuthor2, stmt))
			return "one";
		if (twoDegree(tarAuthor1, tarAuthor2, stmt))
			return "two";
		return "more than two";
	}

	private boolean oneDegree(String name1, String name2,
			Statement stmt) {
		String sql = "select *"
				+ " from writes w"
				+ " where w.name = " + name1
				+ " and w.isbn in ("
				+ " select w.isbn"
				+ " from writes w"
				+ " where w.name = " + name2
				+ ")";
		try {
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
			rs.last();
			int size = rs.getRow();
			rs.close();

			return (size > 0);
		} catch (Exception e) {
			System.out.println("cannot execute the query");
			return false;
		}
	}

	private boolean twoDegree(String name1, String name2,
			Statement stmt) {
		String sql = "select * from authors a where exists"
				+ " (select * from writes w where w.name = a.name"
				+ " and w.isbn in ( select w.isbn"
				+ " from writes w2 where w2.name = " + name1 
				+ " ))"
				+ " and exists ( select *"
				+ " from writes w where w.name = a.name"
				+ " and w.isbn in ( select w.isbn"
				+ " from writes w2 where w2.name = " + name2
				+ "))"
				+ " and not exists ( select *"
				+ " from writes w where w.name = " + name1
				+ " and w.isbn in ( select w.isbn from writes w"
				+ " where w.name = " + name2
				+ " ))";
		try {
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
		
			rs.last();
			int size = rs.getRow();
			rs.close();

			return (size > 0);
		} catch (Exception e) {
			System.out.println("cannot execute the query");
			return false;
		}
	}
}
