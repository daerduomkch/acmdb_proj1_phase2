package acmdb;

import java.sql.*;

public class ManagerLogin {
	
	ManagerLogin() {
		
	}
	
	public boolean run(String mid, String psw, Statement stmt) {
		
		// check malicious login, exclude special characters
		for(int i=0; i<mid.length(); ++i) {
			if(mid.charAt(i)=='\'' || mid.charAt(i) == '\"' || mid.charAt(i) == ' ') {
				System.out.println("Error: special character detected!");
				return false;
			}
		}
		
		for(int i=0; i<psw.length(); ++i) {
			if(psw.charAt(i)=='\'' || psw.charAt(i) == '\"' || psw.charAt(i) == ' ') {
				System.out.println("Error: special character detected!");
				return false;
			}
		}
		
		String sql = "select * from manager where mid = '"+mid+"' and psw = '"+psw+"'";
		
		try {
			ResultSet rs = stmt.executeQuery(sql);
			return rs.next();
		} catch (Exception e) {
			System.out.println("cannot execute the query");
		}
		
		return false;
	}
}
