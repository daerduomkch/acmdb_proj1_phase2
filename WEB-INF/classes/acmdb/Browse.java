package acmdb;

import java.sql.ResultSet;
import java.sql.Statement;

public class Browse {
	
	Browse() {
		
	}
	
	public Result run(String tarAuthor, String tarPublisher, String tarTitle, 
			        String tarSubject, int sort, String login, Statement stmt) {
		if (tarAuthor.intern() == "".intern()) tarAuthor = "'%'";
			else tarAuthor = "'%" + tarAuthor + "%'";
		if (tarPublisher.intern() == "".intern()) tarPublisher = "'%'";
			else tarAuthor = "'" + tarPublisher + "'";
		if (tarTitle.intern() == "".intern()) tarTitle = "'%'";
			else tarTitle = "'%" + tarTitle + "%'";
		if (tarSubject.intern() == "".intern()) tarSubject = "'%'";
			else tarSubject = "'" + tarSubject + "'";
		login = "'" + login + "'";
		switch (sort) {
			case(1):
				return sortByYear(tarAuthor, tarPublisher, tarTitle, tarSubject, stmt);
			case(2):
				return sortByFb(tarAuthor, tarPublisher, tarTitle, tarSubject, stmt);
			case(3):
				return sortByUfb(tarAuthor, tarPublisher, tarTitle, tarSubject, login, stmt);
			default:
				return null;
		}
	}

	private Result sortByYear(String tarAuthor, String tarPublisher,
			String tarTitle, String tarSubject, Statement stmt) {
		
		String sql = "select distinct b.isbn, b.title, b.price, b.yop "
				+ "from books b, authors a, writes w where "
				+ "b.publisher like " + tarPublisher
				+ " and b.title like " + tarTitle
				+ " and b.subject like " + tarSubject
				+ " and b.isbn = w.isbn "
				+ " and a.name = w.name"
				+ " and a.name like " + tarAuthor
				+ " order by b.yop";
		try {
			String[] title = {"isbn", "title", "price", "year"};
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
		
			rs.last();
			int size = rs.getRow();
			rs.beforeFirst();

			Object[][] data = new Object[size][4];
		
			int count = 0;
		
			while(rs.next()) {
				++count;
			
				data[count-1][0] = rs.getString("isbn");
				data[count-1][1] = rs.getString("title");
				data[count-1][2] = rs.getFloat("price");
				data[count-1][3] = rs.getInt("yop");
			}
		
			rs.close();

			return Lib.getResultInstance(size, 4, title, data);
		} 
		catch (Exception e) {
			System.out.println("cannot execute the query");
			return null;
		}
	}

	private Result sortByFb(String tarAuthor, String tarPublisher,
			String tarTitle, String tarSubject, Statement stmt) {
		String sql = "select b.isbn, b.title, b.price, avg(fb.rate) as rate"
				+ " from books b, feedbacks fb"
				+ " where b.isbn in ("
				+ " select distinct b.isbn"
				+ " from books b, authors a, writes w"
				+ " where b.publisher like " + tarPublisher
				+ " and b.title like " + tarTitle
				+ " and b.subject like " + tarSubject
				+ " and b.isbn = w.isbn"
				+ " and a.name = w.name"
				+ " and a.name like " + tarAuthor
				+ " ) and fb.isbn = b.isbn"
				+ " group by b.isbn, b.title, b.price"
				+ " order by avg(fb.rate) desc";
		try {
			String[] title = {"isbn", "title", "price", "rate"};
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
		
			rs.last();
			int size = rs.getRow();
			rs.beforeFirst();

			Object[][] data = new Object[size][4];
		
			int count = 0;
		
			while(rs.next()) {
				++count;
			
				data[count-1][0] = rs.getString("isbn");
				data[count-1][1] = rs.getString("title");
				data[count-1][2] = rs.getFloat("price");
				data[count-1][3] = rs.getFloat("rate");
			}
		
			rs.close();

			return Lib.getResultInstance(size, 4, title, data);
		} 
		catch (Exception e) {
			System.out.println("cannot execute the query");
			return null;
		}
	}
	


	private Result sortByUfb(String tarAuthor, String tarPublisher,
			String tarTitle, String tarSubject, String login, Statement stmt) {
		String sql = "select b.isbn, b.title, b.price, avg(fb.rate) as rate"
				+ " from books b, feedbacks fb"
				+ " where b.isbn in ("
				+ " select distinct b.isbn"
				+ " from books b, authors a, writes w"
				+ " where b.publisher like " + tarPublisher 
				+ " and b.title like " + tarTitle
				+ " and b.subject like " + tarSubject
				+ " and b.isbn = w.isbn"
				+ " and a.name = w.name"
				+ " and a.name like " + tarAuthor
				+ " ) and fb.isbn = b.isbn"
				+ " and fb.login in ("
				+ " select t.tarLogin"
				+ " from trusts t"
				+ " where t.login = " + login
				+ " and t.trust = true )"
				+ " group by b.isbn, b.title, b.price"
				+ " order by avg(fb.rate) desc";
		try {
			String[] title = {"isbn", "title", "price", "rate"};
			System.out.println("Executing: "+sql);
			ResultSet rs = stmt.executeQuery(sql);
		
			rs.last();
			int size = rs.getRow();
			rs.beforeFirst();

			Object[][] data = new Object[size][4];
		
			int count = 0;
		
			while(rs.next()) {
				++count;
			
				data[count-1][0] = rs.getString("isbn");
				data[count-1][1] = rs.getString("title");
				data[count-1][2] = rs.getFloat("price");
				data[count-1][3] = rs.getFloat("rate");
			}
		
			rs.close();

			return Lib.getResultInstance(size, 4, title, data);
		} 
		catch (Exception e) {
			System.out.println("cannot execute the query");
			return null;
		}
	}
}
